'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
 
sass.compiler = require('node-sass');
 
gulp.task('sass', function () {
  return gulp.src('assets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('assets/css/'));
});

gulp.task('scripts', function() {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/foundation-sites/dist/js/foundation.min.js',
		'assets/js/main.js'
	])
	.pipe(concat('main.min.js'))
	.pipe(gulp.dest('assets/js/'));
})
 
gulp.task('sass:watch', function () {
  gulp.watch('assets/scss/*.scss', ['sass']);
});