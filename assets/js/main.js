$(function () {
    $(document).foundation();

    $('#icon-nav-open').on('click', function() {
        $('body').addClass('is-open-right');
    });

    $('#icon-nav-close').on('click', function() {
        $('body').removeClass('is-open-right');
    });

    var h = $('.nav-toggle-container-open').height();
    $('.nav-toggle-container-close').height(h);
    var top = h + 50;
    console.log(top);
    $('.side-menu nav').css('top', top);
});